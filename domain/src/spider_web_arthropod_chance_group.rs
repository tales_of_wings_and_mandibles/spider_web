use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize, sqlx::FromRow)]
pub struct Model {
	pub id: i32,
	pub chance_percentage: i32,
	pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ModelWithoutPrimaryKey {
	pub chance_percentage: i32,
	pub name: String,
}
