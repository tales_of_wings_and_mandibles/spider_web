use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize, sqlx::FromRow)]
pub struct Model {
	pub user_id: i32,
	pub last_processing_date: sqlx::types::chrono::NaiveDateTime,
}
