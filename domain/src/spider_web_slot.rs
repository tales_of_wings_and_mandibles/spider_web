use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize, sqlx::FromRow)]
pub struct Model {
	pub spider_web_id: i32,
	pub slot: i32,
	pub arthropod_definition_id: Option<i32>,
	pub arthropod_is_alive: Option<bool>,
}
