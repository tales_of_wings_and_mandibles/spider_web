use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize, sqlx::FromRow)]
pub struct Model {
	pub arthropod_definition_id: i32,
	pub chance_group_id: i32,
}
