use serde::{Deserialize, Serialize};

use crate::spider_web_slot;

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum SpiderWebSlotItem {
	LivingArthropod(i32),
	DeadArthropod(i32),
	Resource(i32),
}

impl From<spider_web_slot::Model> for Option<SpiderWebSlotItem> {
	fn from(spider_web_slot: spider_web_slot::Model) -> Self {
		match (
			spider_web_slot.arthropod_definition_id,
			spider_web_slot.arthropod_is_alive,
		) {
			(None, None) => None,
			(Some(id), Some(true)) => Some(SpiderWebSlotItem::LivingArthropod(id)),
			(Some(id), Some(false)) => Some(SpiderWebSlotItem::DeadArthropod(id)),
			_ => panic!("Unexpected combination of arthropod_definition_id and arthropod_is_alive: {spider_web_slot:?}."),
		}
	}
}
