use std::{collections::HashMap, error::Error, time::Duration};

use axum::{
	extract::{Path, State},
	http::StatusCode,
	response::IntoResponse,
	routing::{get, post},
	Json, Router,
};
use simplelog::{Config, SimpleLogger};
use sqlx::{PgPool, Pool, Postgres};
use tokio::net::TcpListener;
use tokio_cron_scheduler::JobScheduler;

use spider_web_domain::{
	spider_web, spider_web_arthropod_chance, spider_web_arthropod_chance_group, spider_web_slot,
	spider_web_slot_item::SpiderWebSlotItem,
};

mod populate_spider_web_job;
use populate_spider_web_job::PopulateSpiderWebJob;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
	SimpleLogger::init(log::LevelFilter::Info, Config::default())?;
	log::info!("Log system initialized.");

	let db = PgPool::connect("postgres://towam:towam@towam_postgres:5432").await?;

	let state = AppState { db };

	let job_scheduler = JobScheduler::new().await?;

	let populate_spider_web_job = PopulateSpiderWebJob {
		db: state.db.clone(),
		schedule: "0 * * * * *",
		process_spider_webs_older_than: Duration::from_secs(60 * 5),
	};
	populate_spider_web_job.start(&job_scheduler).await?;

	job_scheduler.start().await?;

	let app = Router::new()
		.route(
			"/spider-web/:id",
			get(get_spider_web).post(create_spider_web),
		)
		.route("/spider-web/:id/slot", get(get_spider_web_slots))
		.route(
			"/spider-web/:id/slot/:slot",
			post(add_spider_web_slot).delete(delete_spider_web_slot),
		)
		.route(
			"/spider-web/:id/slot/:slot/item",
			get(get_item_in_spider_web_slot)
				.post(add_item_in_spider_web_slot)
				.delete(delete_item_in_spider_web_slot),
		)
		.route(
			"/arthropod-chance",
			get(get_all_arthropod_chances).post(set_arthropod_chance),
		)
		.route(
			"/arthropod-chance/group",
			get(get_chance_groups_into_response).post(create_chance_group),
		)
		.route("/health", get(health))
		.with_state(state);

	let listener = TcpListener::bind("0.0.0.0:3000").await.unwrap();

	axum::serve(listener, app).await.unwrap();

	Ok(())
}

async fn get_spider_web(Path(id): Path<i32>, State(state): State<AppState>) -> impl IntoResponse {
	let spider_web = sqlx::query_as!(
		spider_web::Model,
		"SELECT * FROM spider_web WHERE user_id = $1",
		id
	)
	.fetch_optional(&state.db)
	.await
	.unwrap();

	match spider_web {
		None => StatusCode::NOT_FOUND.into_response(),
		Some(sw) => Json(sw).into_response(),
	}
}

async fn create_spider_web(
	Path(id): Path<i32>,
	State(state): State<AppState>,
) -> impl IntoResponse {
	let created_spider_web = spider_web::Model {
		user_id: id,
		last_processing_date: sqlx::types::chrono::Local::now().naive_local(),
	};

	sqlx::query!(
		"INSERT INTO spider_web VALUES ( $1 )",
		sqlx::types::Json(&created_spider_web) as _
	)
	.execute(&state.db)
	.await
	.unwrap();

	(StatusCode::CREATED, Json(created_spider_web))
}

async fn get_spider_web_slots(
	Path(id): Path<i32>,
	State(state): State<AppState>,
) -> impl IntoResponse {
	let spider_web_slots: Vec<spider_web_slot::Model> = sqlx::query_as!(
		spider_web_slot::Model,
		"SELECT * FROM spider_web_slot WHERE spider_web_id = $1 ORDER BY slot ASC",
		id
	)
	.fetch_all(&state.db)
	.await
	.unwrap();

	Json(spider_web_slots)
}

async fn add_spider_web_slot(
	Path((id, slot)): Path<(i32, i32)>,
	State(state): State<AppState>,
) -> impl IntoResponse {
	let spider_web_slot = spider_web_slot::Model {
		spider_web_id: id,
		slot,
		arthropod_definition_id: None,
		arthropod_is_alive: None,
	};

	sqlx::query!(
		"INSERT INTO spider_web_slot VALUES ( $1 )",
		sqlx::types::Json(&spider_web_slot) as _
	)
	.execute(&state.db)
	.await
	.unwrap();

	(StatusCode::CREATED, Json(spider_web_slot))
}

async fn delete_spider_web_slot(
	Path((id, slot)): Path<(i32, i32)>,
	State(state): State<AppState>,
) -> impl IntoResponse {
	sqlx::query!(
		"DELETE FROM spider_web_slot WHERE spider_web_id = $1 AND slot = $2",
		id,
		slot
	)
	.execute(&state.db)
	.await
	.unwrap();

	println!("Removed slot {slot} of spider web {id}.");

	StatusCode::OK
}

async fn get_item_in_spider_web_slot(
	Path((id, slot)): Path<(i32, i32)>,
	State(state): State<AppState>,
) -> impl IntoResponse {
	let spider_web_slot = sqlx::query_as!(
		spider_web_slot::Model,
		"SELECT * FROM spider_web_slot WHERE spider_web_id = $1 AND slot = $2",
		id,
		slot
	)
	.fetch_one(&state.db)
	.await
	.unwrap();

	let spider_web_slot_item: Option<SpiderWebSlotItem> = spider_web_slot.into();

	Json(spider_web_slot_item)
}

async fn add_item_in_spider_web_slot(
	Path((id, slot)): Path<(i32, i32)>,
	State(state): State<AppState>,
	Json(spider_web_slot_item): Json<SpiderWebSlotItem>,
) -> impl IntoResponse {
	let (arthropod_definition_id, arthropod_is_alive) = match spider_web_slot_item {
		SpiderWebSlotItem::LivingArthropod(arthropod_definition_id) => {
			(arthropod_definition_id, true)
		}
		SpiderWebSlotItem::DeadArthropod(arthropod_definition_id) => {
			(arthropod_definition_id, false)
		}
		SpiderWebSlotItem::Resource(_resource_id) => todo!("Add Resource by ID."),
	};

	sqlx::query!("UPDATE spider_web_slot SET arthropod_definition_id = $1, arthropod_is_alive = $2 WHERE spider_web_id = $3 AND slot = $4", arthropod_definition_id, arthropod_is_alive, id, slot).execute(&state.db).await.unwrap();

	let updated_spider_web_slot = sqlx::query_as!(
		spider_web_slot::Model,
		"SELECT * FROM spider_web_slot WHERE spider_web_id = $1 AND slot = $2",
		id,
		slot
	)
	.fetch_one(&state.db)
	.await
	.unwrap();

	Json(updated_spider_web_slot)
}

async fn delete_item_in_spider_web_slot(
	Path((id, slot)): Path<(i32, i32)>,
	State(state): State<AppState>,
) -> impl IntoResponse {
	sqlx::query!("UPDATE spider_web_slot SET arthropod_definition_id = NULL, arthropod_is_alive = NULL WHERE spider_web_id = $1 AND slot = $2", id, slot).execute(&state.db)
	.await
	.unwrap();

	StatusCode::OK
}

const INVALID_CHANCE_PERCENTAGE_ERROR: (StatusCode, &'static str) = (
	StatusCode::BAD_REQUEST,
	"Chance must be a natural number between 1 and 99 (both included).",
);

async fn get_chance_groups_into_response(State(state): State<AppState>) -> impl IntoResponse {
	Json(get_chance_groups(&state.db).await)
}

async fn get_chance_groups(db: &Pool<Postgres>) -> Vec<spider_web_arthropod_chance_group::Model> {
	sqlx::query_as!(
		spider_web_arthropod_chance_group::Model,
		"SELECT * FROM spider_web_arthropod_chance_group"
	)
	.fetch_all(db)
	.await
	.unwrap()
}

async fn create_chance_group(
	State(state): State<AppState>,
	Json(chance_group_model): Json<spider_web_arthropod_chance_group::ModelWithoutPrimaryKey>,
) -> impl IntoResponse {
	if !(0..100).contains(&chance_group_model.chance_percentage) {
		return INVALID_CHANCE_PERCENTAGE_ERROR.into_response();
	}

	let chance_group_id = sqlx::query!(
		"INSERT INTO spider_web_arthropod_chance_group VALUES ( $1 ) RETURNING id",
		sqlx::types::Json(chance_group_model) as _
	)
	.fetch_one(&state.db)
	.await
	.unwrap()
	.id;

	let inserted_chance_group = sqlx::query_as!(
		spider_web_arthropod_chance_group::Model,
		"SELECT * FROM spider_web_arthropod_chance_group WHERE id = $1",
		chance_group_id
	)
	.fetch_one(&state.db)
	.await
	.unwrap();

	(StatusCode::CREATED, Json(inserted_chance_group)).into_response()
}

async fn set_arthropod_chance(
	State(state): State<AppState>,
	Json(arthropod_chance_model): Json<spider_web_arthropod_chance::Model>,
) -> impl IntoResponse {
	sqlx::query!(
		"INSERT INTO spider_web_arthropod_chance VALUES ( $1 )",
		sqlx::types::Json(&arthropod_chance_model) as _
	)
	.execute(&state.db)
	.await
	.unwrap();

	(StatusCode::CREATED, Json(arthropod_chance_model)).into_response()
}

async fn get_all_arthropod_chances(State(state): State<AppState>) -> impl IntoResponse {
	// A collection of chance group IDs mapped to their chance group `Model`.
	let chance_groups: HashMap<i32, spider_web_arthropod_chance_group::Model> = sqlx::query_as!(
		spider_web_arthropod_chance_group::Model,
		"SELECT * FROM spider_web_arthropod_chance_group"
	)
	.fetch_all(&state.db)
	.await
	.unwrap()
	.into_iter()
	.map(|chance_group| (chance_group.id, chance_group))
	.collect();

	let mut arthropod_chances: HashMap<String, f32> = HashMap::new();

	// Async closures are experimental, so I'll use a regular for loop.
	// TODO Use `fetch` to iter a strema? How do they work?
	for arthropod_chance in sqlx::query_as!(
		spider_web_arthropod_chance::Model,
		"SELECT * FROM spider_web_arthropod_chance"
	)
	.fetch_all(&state.db)
	.await
	.unwrap()
	{
		let chance_group = chance_groups
			.get(&arthropod_chance.chance_group_id)
			.unwrap();

		let chance_group_count: i32 = sqlx::query!(
			"SELECT COUNT(*) FROM spider_web_arthropod_chance WHERE chance_group_id = $1",
			arthropod_chance.chance_group_id
		)
		.fetch_one(&state.db)
		.await
		.unwrap()
		.count
		.unwrap() as i32;

		let chance = (chance_group.chance_percentage as f32 / chance_group_count as f32) / 100.0;

		let scientific_name = arthropod_chance.arthropod_definition_id.to_string();

		arthropod_chances.insert(scientific_name, chance);
	}

	Json(arthropod_chances)
}

async fn health(State(state): State<AppState>) -> impl IntoResponse {
	let is_database_connected =
		sqlx::query_as!(spider_web::Model, "SELECT * FROM spider_web LIMIT 1")
			.fetch_one(&state.db)
			.await
			.is_ok();

	if is_database_connected {
		(StatusCode::OK, "ok")
	} else {
		(StatusCode::SERVICE_UNAVAILABLE, "nok")
	}
}

#[derive(Clone)]
struct AppState {
	db: Pool<Postgres>,
}
