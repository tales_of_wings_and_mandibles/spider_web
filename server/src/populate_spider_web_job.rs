use std::{collections::HashSet, error::Error, time::Duration};

use futures::TryStreamExt;
use rand::{seq::SliceRandom, thread_rng, Rng};
use sqlx::{PgPool, Pool, Postgres};
use tokio_cron_scheduler::{Job, JobScheduler};

use spider_web_domain::{spider_web_arthropod_chance, spider_web_slot};

use crate::get_chance_groups;

pub struct PopulateSpiderWebJob {
	pub db: PgPool,

	/// CRON expression that specifies how often this job will run.
	pub schedule: &'static str,

	/// Spider webs that haven't been processed by this job for the specified
	/// duration will be processed the next time this job runs.
	pub process_spider_webs_older_than: Duration,
}

impl PopulateSpiderWebJob {
	pub async fn start(&self, job_scheduler: &JobScheduler) -> Result<(), Box<dyn Error>> {
		let db = self.db.clone();
		let process_spider_webs_older_than = self.process_spider_webs_older_than;

		let job = Job::new_async(self.schedule, move |_uuid, _lock| {
			let db = db.clone();

			Box::pin(async move {
				log::info!("PopulateSpiderWebJob begins.");

				// The empty slots of spider webs that are eligible to be populated.
				let mut slots = sqlx::query_as!(
					spider_web_slot::Model,
					r#"
SELECT * FROM spider_web_slot
WHERE
	arthropod_definition_id IS NULL
	AND spider_web_id IN (SELECT user_id FROM spider_web WHERE last_processing_date <= $1)"#,
					sqlx::types::chrono::Local::now().naive_local()
						- process_spider_webs_older_than
				)
				.fetch(&db);

				// The IDs of the spider webs that have been processed during this
				// execution of the job. Since I obtain all their empty slots, there might
				// be multiple ones belonging to the same spider web, and I only want to
				// process one per spider web.
				let mut processed_spider_webs: HashSet<i32> = HashSet::new();

				while let Some(slot) = slots.try_next().await.unwrap() {
					let spider_web_id = slot.spider_web_id;

					if !processed_spider_webs.contains(&spider_web_id) {
						sqlx::query!(
							"UPDATE spider_web SET last_processing_date = $1 WHERE user_id = $2",
							sqlx::types::chrono::Local::now().naive_local(),
							spider_web_id
						)
						.execute(&db)
						.await
						.unwrap();

						let arthropod_definition_id =
							Some(get_random_arthropod_definition_id(&db).await);

						// TODO Remove the possibility of arthropods being dead?
						let arthropod_is_alive = Some(rand::thread_rng().gen::<bool>());

						sqlx::query!("UPDATE spider_web_slot SET arthropod_definition_id = $1, arthropod_is_alive = $2 WHERE spider_web_id = $3 AND slot = $4", arthropod_definition_id, arthropod_is_alive, slot.spider_web_id, slot.slot)
							.execute(&db)
							.await
							.unwrap();

						log::info!(
							"Populated slot {} of spider web {spider_web_id}.",
							slot.slot
						);

						processed_spider_webs.insert(spider_web_id);
					}
				}

				log::info!("PopulateSpiderWebJob ends.");
			})
		})?;

		job_scheduler.add(job).await?;

		Ok(())
	}
}

/// Returns the ID of a random arhtropod definition.
async fn get_random_arthropod_definition_id(db: &Pool<Postgres>) -> i32 {
	let dice_roll = thread_rng().gen_range(0..=100);

	// Get all the chance groups sorted by their chance percentage in ascending order.
	let sorted_chance_groups_by_chance = {
		let mut chance_groups = get_chance_groups(db).await;
		chance_groups.sort_unstable_by_key(|cg| cg.chance_percentage);

		chance_groups
	};

	// Get the chance group that was rolled.
	let rolled_chance_group = sorted_chance_groups_by_chance
		.iter()
		.find(|cg| dice_roll <= cg.chance_percentage)
		.unwrap();

	// Get a random arthropod definition ID from the rolled chance group.
	sqlx::query_as!(
		spider_web_arthropod_chance::Model,
		"SELECT * FROM spider_web_arthropod_chance WHERE chance_group_id = $1 ORDER BY random()",
		rolled_chance_group.id
	)
	.fetch_all(db)
	.await
	.unwrap()
	.choose(&mut thread_rng())
	.unwrap()
	.arthropod_definition_id
}

// TODO Instead of having to use the `processed_spider_webs` `HashSet`, I could
// make the SQL query keep a single empty slot directly. This works:

// SELECT DISTINCT ON (spider_web_slot.spider_web_id) * FROM spider_web_slot
// WHERE
// 	spider_web_slot.spider_web_id IN (
// 		SELECT user_id FROM spider_web
// 			WHERE
// 				...
// 	)
// 	AND spider_web_slot.arthropod_definition_id IS NULL
// ;

// How can I replicate this with `sea_orm`? I could use a compile-time checked
// `sqlx` query instead, too.
