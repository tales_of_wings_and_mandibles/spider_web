FROM debian:testing-slim

COPY target/debug/spider_web spider_web

CMD ["./spider_web"]
